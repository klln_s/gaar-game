# GODDAMN ASTEROIDS & ALIENS ! REMASTERED - GAME

<p align="center">
<img src="Screenshots/GAAR_logo.png" />
</p>

####  This project was part of my 2nd year of Prep cycle at Polytech Orléans (accredited faculty of public engineering). I made it in 2 weeks (program + report + poster) with MonoGame Framework in C#. It's a modern version of the famous game Asteroids. Please be advised that this project was made before my first year of computer engineering cycle. With my current knowledge and experience, A LOT OF THINGS in the codebase hurt my eyes... I'll probably make a 2024 version of this project with a whole new code design/code base, new features, new sprites, musics... etc !

> Video game made in  **C#** with the **MonoGame Framework**

**Link to see a video of the game incoming !!!**

<p align="center">
<img src="Screenshots/backgroundInfos.png"/>
</p>
<p align="center">
<img src="Screenshots/asteroid_fat.png"/>
</p>

<p align="center">
<img src="Screenshots/asteroid_medium.png"/>
</p>

<p align="center">
<img src="Screenshots/asteroid_little.png"/>
</p>

<p align="center">
<img src="Screenshots/player_sprite.png"/>
</p>